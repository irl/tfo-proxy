/**
 * proxy.c - Main application source
 *
 * This file is part of the TCP Fast Open Proxy
 *
 * Copyright (C) Iain R. Learmonth and contributors 2013
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <errno.h>

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/socket.h>

#include "fastopen.h"

int main(int argc, char **argv) {

	int sockfd, port, n;
	struct sockaddr_in server_address;
	struct hostent *server;

	if(argc < 5) {
		printf("Usage:\n");
		printf("%s {proxy interface} {proxy port} {application interface} {application port}", argv[0]);
		exit(0);
	}

	printf("Preparing listening socket... ");

	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if ( errno == 0 ) {
		printf("done\n");
	} else {
		printf("failed\nError: %s\n", strerror(errno));
	}

	memset(&server_address, 0, sizeof(struct sockaddr_in));
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = inet_addr(argv[1]);
	server_address.sin_port = htons(atoi(argv[2]));

	int qlen = 5;

	printf("Binding socket...");

	bind(sockfd, (struct sockaddr *)&server_address, sizeof(struct sockaddr));

	if ( errno == 0 ) {
		printf("done\n");
	} else {
		printf("failed\nError: %s\n", strerror(errno));
	}

	printf("Setting TCP_FASTOPEN flag...");

	setsockopt(sockfd, SOL_TCP, TCP_FASTOPEN, &qlen, sizeof(qlen));

	if ( errno == 0 ) {
		printf("done\n");
	} else {
		printf("failed\nError: %s\n", strerror(errno));
	}

	printf("Activating listener(s)...");

	listen(sockfd, 1);

	if ( errno == 0 ) {
		printf("done\n");
	} else {
		printf("failed\nError: %s\n", strerror(errno));
	}

	socklen_t socksize = sizeof(struct sockaddr_in);
	struct sockaddr_in dest;

	char buf_fromclient[256];
	char buf_toclient[256];

	int connection = accept(sockfd, (struct sockaddr *)&dest, &socksize);

	while(connection) {
		int len = recv(connection, buf_fromclient, 128, 0);
		printf("[RECV] Length: %d Error Code: %d Error: %s\n", len, errno, strerror(errno));
		buf_fromclient[len] = 0;
		printf("%s", buf_fromclient);
		if(len == -1 || len == 0) {
			close(connection);
			connection = accept(sockfd, NULL, 0);
		}
		
	}

	close(sockfd);

}
